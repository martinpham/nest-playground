import { Module } from '@nestjs/common';

import { LocalProcessor } from '@worker/processors/local';
import { PrismaService } from '@libs/prisma';
import { createModule } from '@libs/bull';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    createModule('local')
  ],
  controllers: [],
  providers: [
    PrismaService,
    LocalProcessor
  ],
})
export class WorkerModule {}
