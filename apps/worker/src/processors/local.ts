import { PrismaService } from '@libs/prisma';
import { Processor, Process } from '@nestjs/bull';
import { Job } from 'bull';

@Processor('local')
export class LocalProcessor {
  constructor(
    private readonly prisma: PrismaService
  ) {}

  
  @Process()
  async process(
    job: Job<unknown>
  ) {

    console.log(await this.prisma.user.findMany({
      
    }));

    console.log(job)
    
  }
}
