import { Controller, Get } from '@nestjs/common';
import { PrismaService } from '@libs/prisma';

@Controller()
export class HomeController {
  constructor(
    private readonly prisma: PrismaService
  ) {}

  @Get()
  getHello() {
    return this.prisma.user.findMany({
      
    });
  }
}
