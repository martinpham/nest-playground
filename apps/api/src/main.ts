import { NestFactory } from '@nestjs/core';
import { ApiModule } from './api.module';

const PORT = process.env.PORT || 3000;

async function bootstrap() {
  const app = await NestFactory.create(ApiModule);
  await app.listen(PORT);
}
bootstrap();
