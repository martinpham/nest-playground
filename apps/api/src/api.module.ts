import { Module } from '@nestjs/common';
import { HomeController } from '@api/controllers/home';
import { PrismaService } from '@libs/prisma';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule.forRoot()],
  controllers: [HomeController],
  providers: [PrismaService],
})
export class ApiModule {}
