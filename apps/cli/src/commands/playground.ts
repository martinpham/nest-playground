import { Command, Positional, Option } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '@libs/prisma';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { BullModule } from '@nestjs/bull';

const delay = async (ms: number) =>
  new Promise((resolve, reject) => setTimeout(resolve, ms));

  
@Injectable()
export class PlaygroundCommand {
  constructor(
    private readonly prisma: PrismaService,
    @InjectQueue('local') private audioQueue: Queue,
  ) {}

  @Command({
    command: 'playground',
    describe: 'playground command',
  })
  async create() {
    console.log(await this.prisma.user.findMany({}));

    await delay(1000);
    console.log('queue1xx');
    this.audioQueue.add({
      foo: 'xxx1',
      bar: 'yyy2',
    });

    await delay(3000);

    console.log('queue2xz');
    this.audioQueue.add({
      foo: 'aaa1',
      bar: 'bbb2',
    });
  }
}
