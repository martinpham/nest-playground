import { Module } from '@nestjs/common';
import { CommandModule } from 'nestjs-command';
import { PlaygroundCommand } from '@cli/commands/playground';
import { PrismaService } from '@libs/prisma';
import { createModule } from '@libs/bull';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    CommandModule,
    createModule('local')
  ],
  controllers: [],
  providers: [PrismaService, PlaygroundCommand],
})
export class CliModule {}

