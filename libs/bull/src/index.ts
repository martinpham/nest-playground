import { BullModule } from '@nestjs/bull';

export const createModule = (queueName) => {
  return BullModule.registerQueue({
    name: queueName,
    redis: {
      host: process.env.QUEUE_REDIS_HOST,
      port: Number(process.env.QUEUE_REDIS_PORT),
    },
  });
};
